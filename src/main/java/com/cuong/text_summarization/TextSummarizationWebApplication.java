package com.cuong.text_summarization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TextSummarizationWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(TextSummarizationWebApplication.class, args);
	}

}
