package com.cuong.text_summarization.models;

public class SummaryParam {

	private String document;
	private Long summarySize;

	public SummaryParam() {
		super();
	}

	public SummaryParam(String document, Long summarySize) {
		super();
		this.document = document;
		this.summarySize = summarySize;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public Long getSummarySize() {
		return summarySize;
	}

	public void setSummarySize(Long summarySize) {
		this.summarySize = summarySize;
	}

}
