package com.cuong.text_summarization.models;

public class Summary {

	private String summaryMethod;
	private String summary;
	private Long summarySize;
	private int status;
	private String message;

	public Summary() {
		super();
	}

	public Summary(String summaryMethod, String summary, Long summarySize, int status, String message) {
		super();
		this.summaryMethod = summaryMethod;
		this.summary = summary;
		this.summarySize = summarySize;
		this.status = status;
		this.message = message;
	}

	public String getSummaryMethod() {
		return summaryMethod;
	}

	public void setSummaryMethod(String summaryMethod) {
		this.summaryMethod = summaryMethod;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Long getSummarySize() {
		return summarySize;
	}

	public void setSummarySize(Long summarySize) {
		this.summarySize = summarySize;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
