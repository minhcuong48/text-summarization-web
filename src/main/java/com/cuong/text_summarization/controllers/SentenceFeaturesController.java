package com.cuong.text_summarization.controllers;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cuong.preprocessors.parsers.TokenParser;
import com.cuong.text_summarization.models.Summary;
import com.cuong.text_summarization.models.SummaryParam;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.SentenceFeatureSummaryTool;

@Controller
@RequestMapping("/sentence-features")
public class SentenceFeaturesController {
	
	@GetMapping("/new")
	public String new_() {
		return "sentence_features/new";
	}
	
	@PostMapping("/")
	@ResponseBody
	public String create(@RequestBody String summaryParam) throws IOException {
		ObjectMapper paramMapper = new ObjectMapper();
		SummaryParam param = paramMapper.readValue(summaryParam, SummaryParam.class);
		
		String document = param.getDocument();
		Long summarySize = param.getSummarySize();
		
		Summary summary = null;
		try {
			String summaryString = SentenceFeatureSummaryTool.summaryUsingAllFeatures(document, summarySize.intValue());
			TokenParser tokenParser = new TokenParser();
			tokenParser.parse(summaryString);
			Long wordCount = tokenParser.countWordsExcludePunct();
			summary = new Summary("Sentence Features", summaryString, wordCount, 1, null);
		} catch(Exception e) {
			summary = new Summary("Sentence Features", null, new Long(0), 0, "Can not summarize this document!");
		}
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonResult = mapper.writeValueAsString(summary);
		return jsonResult;
	}
	
}
