package com.cuong.text_summarization.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import textrank.apps.TextRank;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@GetMapping("/")
	public String home() {
		String document = "Anh yêu em nhiều lắm. Thu Uyên ơi. Tình yêu của anh";
		String summary = TextRank.summarize(document, 3);
		System.out.println(summary);
		return "home/index";
	}
	
}
